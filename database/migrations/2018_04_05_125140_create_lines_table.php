<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->text('content');
            $table->char('tag')->nullable();
            $table->boolean('delete')->default(false);
            $table->timestamps();
        });

        DB::statement('ALTER TABLE `lines` ADD FULLTEXT `search` (`tag`, `content`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lines', function($table){
            $table->dropIndex('search');
        });
        Schema::dropIfExists('lines');
    }
}
