<?php
namespace TimeLine\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \interop\Container\ContainerInterface;

class BaseController {
    protected $ci;

    public function __construct(ContainerInterface $ci) {
        $this->ci = $ci;
    }

    public function methodExample($request, $response, $args) {
        //your code
        //to access items in the container... $this->ci->get('');
        // return $this->ci->view->render($response, 'index.html');
    }

}