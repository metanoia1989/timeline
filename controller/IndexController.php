<?php
namespace TimeLine\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \interop\Container\ContainerInterface;

class IndexController extends BaseController {
    protected $ci;

    public function index($request, $response, $args) {
        return $this->ci->view->render($response, 'index.html');
    }
}