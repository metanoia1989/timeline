<?php
namespace TimeLine;

require_once "./vendor/autoload.php";
require_once './common/config.php';


use \Psr\Http\Message\ServerRequestInterface as Request;  
use \Psr\Http\Message\ResponseInterface as Response;  
use Medoo\Medoo;

$app = new \Slim\App($config);
$container = $app->getContainer();

//注入medoo数据库类以及twig模板引擎
$container['db'] = function ($config) {
    return new Medoo($config['settings']['db']);
};

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('./templates', [
        // 'cache' => './cache'
        'cache' => false 
    ]);
    $basePath = rtrim(str_ireplace("index.php", "", $container["request"]->getUri()->getBasePath()), "/");
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $basePath
    ));

    $view->getEnvironment()->addGlobal('root_path', $basePath);
    $view->getEnvironment()->addGlobal('current_path', $container['request']->getUri()->getPath());
    return $view;
};

//定义路由
$app->get('/', function(Request $request, Response $response, $args) {
    return $response->write("这是timeline的首页");
});

$app->get('/test', function(Request $request, Response $response, $args) {
    return $response->write(SLIM_ROOT);
});

$app->get('/index', 'TimeLine\Controller\IndexController:index');

$app->run();
