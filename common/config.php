<?php 
define('SLIM_ROOT', dirname(__DIR__));

$config =  [
    'settings' => [
        'displayErrorDetails' => true, //开启debug模式
        'db' => [
            'database_type' => 'mysql',
            'database_name' => 'cosmi',
            'server' => 'localhost',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ],
        'site_root' => SLIM_ROOT,
    ],
];